#!/usr/bin/env python
'''
	install zeromq libraries
'''

import shutil

shutil.copyfile("zmq.hpp", "/usr/local/include/zmq.hpp")
shutil.copyfile("zhelpers.hpp","/usr/local/include/zhelpers.hpp")
shutil.copyfile("zmsg.hpp","/usr/local/include/zmsg.hpp")
